#!/bin/bash

source .env

mkdir -p backend

# dumping the enviroment variables to the .json
cat > "backend/config.json" << EOL
{
	"title": "Pic4Review API configuration file",
	"base_url": "${BASEURL}",
	"db": {
		"host": "${HOST}",
		"user": "${POSTGRES_USER}",
		"pass": "${POSTGRES_PASSWORD}",
		"name": "p4r_db",
		"port": "${PG_PORT}"
	},
	"mail": {
		"from": {
			"address": "${EMAIL_ADDRESS_SENDER}",
			"server": "${EMAIL_SERVER}",
			"password": "${EMAIL_PASSWORD}",
			"secure": "${EMAIL_PASSWORD}"
		},
		"to": {
			"default": "${EMAIL_ADDRESS_RECEIVER}"
		}
	},
	"pictureFetchersCredentials": {
		"mapillary": { "key": "${MAPPILARY_KEY}" }
	},
	"overpass": "${OVERPASS_ADDR}",
	"timers": {
		"firstMissionUpdate": "${F_MISSION_UPDATE}",
		"delayBetweenMissionUpdate": "${DELAY_B_M_UPDATE}"
	},
	"cameraAngle": "${CAMERANGLE}"
}
EOL
