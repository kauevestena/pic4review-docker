# About

Created to help on development of Pic4Review from Adrien Pavie, putting into a single image both Frontend and Backend

Here must be only the code to generate the image, the image itself shall be at DockerHub

# Requeriments

 - docker
 - docker-compose

# How to:

copy the file "dot_env_example", saving it as ".env" and setup the variables there. It will held secrets like the database password, and mappilary api key, thereby it's listed at ".gitignore"

- warning: watch out for already used ports, specially if you already have any pgsql database running at the default 5432, you can then set another one at .env as PG_PORT

then the "config.json" shall be created using the "write_config.bash":

    bash write_config.bash

check if it's everything allright (any .json linter will do the job) with "Pic4Review-backend/config.json"

after everything is set up, just run docker compose:

    docker-compose up

# Updating

if you update the source code of backend and/or frontend you can then rebuild the docker image using:

    docker-compose up --build

if some container changed its name the "--remove-orphans" flag might be necessary aswell

# changing repos 

 you can run
 
    sh cloning_repos.sh
    
to switch to your fork rather than another one (like the original from Adrien)

# troubleshooting

When try to run "docker compose up":

- Couldn't connect to Docker daemon at http+docker://localhost ...


1. Add user to docker group (if not already added)
   
    sudo groupadd docker
    sudo usermod -aG docker $USER


2. create a symbolic link to /usr/bin using the following command
   
    sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

3. Restart docker service:

    sudo service docker restart

    [source](https://forums.docker.com/t/couldnt-connect-to-docker-daemon-at-http-docker-localhost-is-it-running/87257/2)

As [pointed here](https://stackoverflow.com/a/52798075) you may also need to:

   sudo chown $USER /var/run/docker.sock